import EntryFormModal from '../cycle/EntryFormModal';
import React from 'react';
import {connect} from 'react-redux';

const MODAL_COMPONENTS = {
	ENTRY_FORM: EntryFormModal
	/* other modals */
};

function ModalRoot({ modalType, modalProps }){
	if (!modalType) {
		return null;
	}
	const SpecificModal = MODAL_COMPONENTS[modalType];
	return <SpecificModal {...modalProps} />;
}
export default connect(state => state.modal)(ModalRoot);
