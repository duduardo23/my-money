#!/bin/bash

FOLDER=$( cd $(dirname $0) ; pwd -P )
FRONTEND_DEST=$FOLDER/docker-volumes/html
echo "===========Start build frontend==========="
echo Front end folder: $FRONTEND_DEST
mkdir -p $FRONTEND_DEST 

docker run -it --rm \
    --user "$(id -u):$(id -g)" \
    -v $FOLDER/client:/workspace \
    -v $FRONTEND_DEST:/workspace/build \
    --name fronten_builder node:8-alpine sh -c 'cd /workspace \
        && yarn \
        && env NODE_OPTIONS=--max_old_space_size=512 yarn build'

echo "===========End build frontend==========="
