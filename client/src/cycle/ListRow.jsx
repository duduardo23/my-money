import React, { Component } from 'react';
import currencyFormatter from '../common/util/currencyFormatter';
import If from '../common/operator/If';
import {Button, ButtonGroup, DropdownButton, MenuItem} from 'react-bootstrap';
import StatusButton from './StatusButton';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {updateDebtStatus, removeEntry} from './cycleActions';
import RowForm from './RowForm';
import {editEntryModal} from '../modal/modalActions';
class ListRow extends Component {
	state = {
		edit : false,
	}
	renderDate(date, status){
		if(!date || (this.props.type === "Debt" && status !== 'PAYD')){
			return null;
		}
		return (
			<span>{date}</span>
		);
	}
	renderRecurrent(item){
		if(!item.recurrent){
			return null;
		}
		if(item.recurrentCount > 0){
			return (
				<span>{item.recurrentCount + '/' + item.recurrentTotal}</span>
			);
		}
		return ( <span>Sim</span> );
	}

	render(){
		if(this.state.edit){
			return this.renderRowForm();
		}
		return this.renderRowData();
	}

	edit(){
		this.props.editEntryModal(this.props.type, this.props.item.id);
	}

	updateDebitStatus(newStatus){
		this.props.updateDebtStatus(this.props.item.id, newStatus);
	}

	renderRowForm(){
		return (
			<tr>
				<td colSpan={this.props.type === "Debt" ? 7 : 6} >
					<RowForm type={this.props.type} item={this.props.item}/>
				</td>
			</tr>
		);
	}
	remove(){
		if(window.confirm('Esta operação não podera ser desfeita, deseja continuar?')){
			this.props.removeEntry(this.props.type, this.props.item.id);
		}
	}
	renderRowData() {
		const item = this.props.item;
		return (
			<tr>
				<td>{this.renderDate(item.formattedDate, item.status)}</td>
				<td>{item.name}</td>
				<td>
					<If test={item.tag}>
						<Button bsSize="xs" bsStyle="info"
							onClick={() => this.props.selectTag(item.tag)}
						>{'#' + item.tag}</Button>
					</If>
				</td>
				<td>{currencyFormatter(item.value)}</td>
				<td>{this.renderRecurrent(item)}</td>
				<If test={this.props.type === "Debt"}>
					<td><StatusButton item={item} updateStatus={newStatus => this.updateDebitStatus(newStatus)}/></td>
				</If>
				<td>
					<ButtonGroup bsSize="xs">
						<Button bsSize="xs" onClick={() => this.edit()}>
							<i className="fa fa-pencil" />
						</Button>
						<DropdownButton title="" id="bg-nested-dropdown" bsSize="xs">
							<MenuItem eventKey="1" onClick={() => this.remove()}>
								<i className="fa fa-trash" /> Excluir
							</MenuItem>
						</DropdownButton>
					</ButtonGroup>
				</td>
			</tr>
		);
	}
}

const mapDispatchToProps = (dispach) => {
	return bindActionCreators({ updateDebtStatus, editEntryModal, removeEntry}, dispach);
};
export default connect(null, mapDispatchToProps)(ListRow);
