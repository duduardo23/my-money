import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import Dashboard from "../dashboard/Dashboard";
import About from "../about/About";
import BillingCicle from "../billingCycle/BillingCycle";
import ImportBank from "../import/ImportBank";
import Cycle from "../cycle/Cycle";

export default function Routes(props) {
	return (
		<Switch>
			<Route exact path="/" component={Dashboard} />
			<Route exact path="/about" component={About} />
			<Route exact path="/billingCycles" component={BillingCicle} />
			<Route exact path="/importar-extrato" component={ImportBank} />
			<Route exact path="/ciclo" component={Cycle} />
			<Redirect from="*" to="/" />
		</Switch>
	);
}
