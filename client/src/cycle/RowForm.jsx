import React, { Component } from 'react';
import If from '../common/operator/If';
import {Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateDebt, updateCredit } from './cycleActions';

class RowForm extends Component {


	constructor(props){
		super(props);
		this.onChange = this.onChange.bind(this);
	}
	formateDate(date){
		const day = date.getDate();
		const month = date.getMonth() + 1;
		return `${date.getFullYear()}-${(month > 9 ? month : '0' + month)}-${(day > 9 ? day : '0' + day)}`;
	}
	componentWillMount(){
		const {entryDate, name, value, tag, status, recurrent, recurrentCount, recurrentTotal} = this.props.item;

		const dt = new Date(entryDate);

		this.setState({
			entryDate: this.formateDate(dt),
			name,
			value,
			tag: tag || '',
			status,
			recurrent,
			recurrentCount: recurrentCount || '',
			recurrentTotal: recurrentTotal || ''
		});
	}

	updateItem(){
		const data = {
			...this.state,
			recurrentCount: this.state.recurrentCount || 0,
			recurrentTotal: this.state.recurrentTotal || 0
		};
		const fnc = (this.props.type === "Debt") ? this.props.updateDebt : this.props.updateCredit;
		fnc(this.props.item.id, data);
		this.setState({edit: false});
	}

	onChange(e){
		const target = e.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const newState = {};
		newState[name] = value;
		this.setState(newState);
	}

	render() {
		return (
			<div className="row-form">
					<input type="date"
						  name="entryDate" id="entryDate" className="th-date form-control input-sm"
						  value={this.state.entryDate} onChange={this.onChange}
						  placeholder="Data" ref="inptEntryDate"
    				/>
					<input type="text" name="name" id="name" className="inpt-name form-control input-sm"
						value={this.state.name} onChange={this.onChange} required
						placeholder="Nome"/>
					<input type="text" name="tag" id="tag" className="th-date form-control input-sm"
						value={this.state.tag} onChange={this.onChange}
						placeholder="Tag"/>
					<input type="number" name="value" id="value" className="th-value form-control input-sm"
						value={this.state.value} onChange={this.onChange} required
						placeholder="Valor" min="0" step="0.1"/>

					<input type="checkbox" name="recurrent" className="th-value"
						checked={this.state.recurrent} onChange={this.onChange}
						/>
					<If test={this.state.recurrent}>
						<input type="text" name="recurrentCount" id="recurrentCount" className="inpt-rec form-control input-sm"
							value={this.state.recurrentCount} onChange={this.onChange} required
							/>
						/
						<input type="text" name="recurrentTotal" id="recurrentTotal" className="inpt-rec form-control input-sm"
							value={this.state.recurrentTotal} onChange={this.onChange} required
							/>
					</If>
					<If test={this.props.type === "Debt"}>
							<select name="status" className="form-control input-sm inpt-status"
								value={this.state.status} onChange={this.onChange}
								placeholder="Status">
								<option value="PAYD">Pago</option>
								<option value="SCHEDULED">Agendado</option>
								<option value="PENDING">Pendente</option>
							</select>
					</If>
					<Button bsSize="xs" bsStyle="success" onClick={() => this.updateItem()}>
						<i className="fa fa-check" aria-hidden="true"></i>
					</Button>
					<Button bsSize="xs" onClick={() => this.setState({edit: false})}>
						<i className="fa fa-ban" />
					</Button>
					</div>
		);
	}
}

const mapDispatchToProps = (dispach) => {
	return bindActionCreators({ updateDebt, updateCredit }, dispach);
};
export default connect(null, mapDispatchToProps)(RowForm);
