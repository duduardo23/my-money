import React from "react";
import If from '../operator/If';
import PropTypes from 'prop-types';
function Button(props) {
	let classes = "btn";
	classes += props.color ? ' btn-' + props.color : " btn-default";
	if (props.size) {
		classes += " btn-" + props.size;
	}
	if (props.disable) {
		classes += " disable";
	}
	if (props.block) {
		classes += " btn-block";
	}
	if (props.flat) {
		classes += " btn-flat";
	}

	if(props.className){
		classes += ' ' + props.className;
	}

	return (
		<button type="button" className={classes} disabled={props.disabel} onClick={props.onClick}>
			<If test={props.icon}>
				<i className={`fa fa-${props.icon}`} />{' '}
			</If>
			{props.label}
			{props.children}
		</button>
	);
}

Button.propTypes = {
	size: PropTypes.oneOf('lg', 'sm', 'xs'),
	color: PropTypes.oneOf('primay', 'success', 'info', 'danger', 'warning')
};

export default Button;
