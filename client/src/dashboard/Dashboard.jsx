import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ContentHeader from '../common/template/ContentHeader';
import Content from '../common/template/Content';
import ValueBox from '../common/widgets/ValueBox';
import Row from '../common/layout/Row';

import { getSummary } from './dashboardActions';
import CreditDebitChart from './CreditDebtChart';
import Grid from '../common/layout/Grid';


class Dashboard extends Component {
	componentWillMount(){
		this.props.getSummary();
	}
	render() {
		const { credit, debt } = this.props.summary || {};
		return (
			<div>
				<ContentHeader title='Dashboard' small='Versão 1.0' />
				<Content>
					<Row>
						<ValueBox cols='12 4' color='green' icon='bank'
							value={credit} text='Total de Créditos' />
						<ValueBox cols='12 4' color='red' icon='credit-card'
							value={debt} text='Total de Débitos' />
						<ValueBox cols='12 4' color='blue' icon='money'
							value={credit - debt} text='Valor Consolidade' />
					</Row>
					<section class="content">
						<Row>
							<Grid cols="6">
								<CreditDebitChart /	>
							</Grid>
						</Row>
					</section>
				</Content>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	summary: state.dashboard.summary
});
const mapDispatchToProps = (dispach) => {
	return bindActionCreators({ getSummary }, dispach);
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
