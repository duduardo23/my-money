import React, { Component } from 'react';
import ContentHeader from '../common/template/ContentHeader';
import Content from '../common/template/Content';

import Tabs from '../common/tab/Tabs';
import TabsHeader from '../common/tab/TabsHeader';
import TabsContent from '../common/tab/TabsContent';
import TabHeader from '../common/tab/TabHeader';
import TabContent from '../common/tab/TabContent';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BillingCycleList from './BillingCycleList';
import BillingCycleForm from './BillingCycleForm';

import { create, update, remove, init} from "./billingCyclesActions";


class BillingCycle extends Component {
	componentWillMount() {
		this.props.init();
	}
	render() {
		return (
			<div>
				<ContentHeader title='Ciclos de pagamentos' small='Cadastro' />
				<Content>
					<Tabs>
						<TabsHeader>
							<TabHeader label='Listar' icon='bars' target='tabList' />
							<TabHeader label='Incluir' icon='plus' target='tabCreate' />
							<TabHeader label='Alterar' icon='pencil' target='tabUpdate' />
							<TabHeader label='Excluir' icon='trash-o' target='tabDelete' />
						</TabsHeader>
						<TabsContent>
							<TabContent id='tabList'>
								<BillingCycleList/>
							</TabContent>
							<TabContent id='tabCreate'>
								<BillingCycleForm onSubmit={this.props.create} submitLabel="Incluir" submitClass="primary" />
							</TabContent>
							<TabContent id='tabUpdate'>
								<BillingCycleForm onSubmit={this.props.update} submitLabel="Alterar" submitClass="info" />
							</TabContent>
							<TabContent id='tabDelete'>
								<BillingCycleForm onSubmit={this.props.remove} submitLabel="Remover" submitClass="danger" readOnly={true}/>
							</TabContent>
						</TabsContent>
					</Tabs>
				</Content>

			</div>
		);
	}
}
const mapDispatchToPros = dispach => bindActionCreators({ create, update, remove, init}, dispach);
export default connect(null, mapDispatchToPros)(BillingCycle);
