import Consts from "./Consts";

export function showModal(modalType, modalProps){
	return {
		type: Consts.SHOW_MODAL,
		modalType: modalType,
		modalProps: modalProps
	};
}
export function hideModal(){
	return {
		type: "HIDE_MODAL"
	};
}

export function showEntryModal(modalProps = {}){
	return showModal(Consts.ENTRY_FORM, modalProps);
}

export function newEntryModal(type){
	return showEntryModal({type});
}
export function editEntryModal(type, id){
	return showEntryModal({type, id});
}


