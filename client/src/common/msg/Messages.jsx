import React from "react";
import ReduxToastr from "react-redux-toastr";

export default function Messages(props) {
	return (
		<ReduxToastr
			timeOut={8000}
			newestOnTop={true}
			preventDuplicates={true}
			position="top-right"
			transitionIn="fadeIn"
			transitionOut="fadeOut"
			progressBar
		/>
	);
}
