
import consts from '../consts';

export function getEntryApiUrl(type, id){
	const baseUrl = {
		'Debt': 'debt',
		'Credit' : 'credit',
		'CreditCard' : 'creditCard',
		'Investiment' : 'investiment'
	}[type];
	if(id){
		return `${consts.API_URL}/${baseUrl}/${id}`;
	}
	return `${consts.API_URL}/${baseUrl}`;
}
