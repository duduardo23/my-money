import React, { Component } from "react";
import "./List.css";
import {Button, Pagination} from "react-bootstrap";
import If from "../common/operator/If";
import ListRow from "./ListRow";
class List extends Component {
	state = {
		tag: ''
	}
	constructor(props){
		super(props);
		this.changeItemsPerPage = this.changeItemsPerPage.bind(this);
		this.changePage = this.changePage.bind(this);
		this.selectTag = this.selectTag.bind(this);
	}
	renderHeader(){
		return (
			<tr>
				<th className="th-date">Data</th>
				<th>Nome</th>
				<th>#</th>
				<th className="th-value">Valor</th>
				<th className="th-recorrent">Rec...</th>
				<If test={this.props.type === "Debt"}>
					<th className="th-recorrent">Status</th>
				</If>
				<th className="th-option" />
			</tr>
		);
	}

	renderRows() {
		const list = this.props.data.items || [];
		return list.map(item => (
			<ListRow key={item.id} item={item} selectTag={this.selectTag} type={this.props.type}/>
		));
	}

	renderPagination(){
		const {totalPages, page} = this.props.data;
		if(totalPages <=1 ){
			return null;
		}
		const items = [];
		items.push(<Pagination.First key="first" onClick={() => this.changePage(1)}/>);
		items.push(<Pagination.Prev key="prev" onClick={() => this.changePage( page - 1 )}/>);
		for (let number = 1; number <= totalPages; number++) {
			items.push(
				<Pagination.Item key={number} active={number === page}
					onClick={() => this.changePage(number)}>{number}</Pagination.Item>
				);
		}
		items.push(<Pagination.Next key="next" onClick={() => this.changePage( page + 1 )}/>);
		items.push(<Pagination.Last key="last" onClick={() => this.changePage(totalPages)}/>);

		return items;
	}

	selectTag(tag){
		const { itemsPerPage } = this.props.data;
		this.props.onChangeData(1, itemsPerPage, tag);

	}

	changePage(page){
		const { itemsPerPage } = this.props.data;
		this.props.onChangeData(page, itemsPerPage, this.props.data.tag);
	}

	changeItemsPerPage(e){
		const newValue = e.target.value;
		this.props.onChangeData(1, newValue, this.props.data.tag);
	}

	renderTagsOptions(){
		const {tags} = this.props.data;
		if(!tags){
			return null;
		}
		return tags.map(tag => (<option key={tag} value={tag}>{tag}</option>));
	}

	render() {
		return (
			<div className={`panel box box-${this.props.boxStyle}`}>
				<div className="box-header with-border">
					<h3 className="box-title">{this.props.title}</h3>
					<div className="pull-right form-inline">
						<label className="lbl-items-page">
							Tag:{' '}
							<select name="tag" className="form-control input-sm"
							  onChange={(e) => this.selectTag(e.target.value)}
							  value={this.props.data.tag}>
								<option value="">&nbsp;</option>
								{this.renderTagsOptions()}
							</select>
						</label>
					</div>
				</div>
				<div className="box-body no-padding">
					<table className="table table-condensed">
						<thead>
							{this.renderHeader()}
						</thead>
						<tbody>
							{this.renderRows()}
						</tbody>
					</table>
				</div>
				<div className="box-footer clearfix">
					<div className="no-margin pull-left form-inline dataTables_wrapper">
						<div className="dataTables_length">
							<label className="lbl-items-page">
								Exibir {' '}
								<select name="example1_length" aria-controls="example1"
									className="form-control input-sm"
									onChange={this.changeItemsPerPage}>
										<option value="10">10</option>
										<option value="15">15</option>
										<option value="20">20</option>
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
								</select>{' '}items. Total: {this.props.data.totalItems}.
							</label>
						</div>
					</div>
					<Pagination bsSize="small" className="no-margin pull-right">
						{this.renderPagination()}
					</Pagination>
				</div>
			</div>
		);
	}
}

export default List;
