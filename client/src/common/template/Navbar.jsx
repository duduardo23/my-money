import React, { Component,  Fragment} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout } from '../../auth/authActions';
import {newEntryModal} from '../../modal/modalActions';
class Navbar extends Component {
	constructor(props) {
		super(props);
		this.state = { open: false };
	}
	changeOpen() {
		this.setState({ open: !this.state.open });
	}
	render() {
		const { name, email } = this.props.user;
		return (
			<Fragment>
				<a href="" className="sidebar-toggle" data-toggle="push-menu" role="button">
					<span className="sr-only">Toggle navigation</span>
				</a>
				<div className="navbar-custom-menu">
					<ul className="nav navbar-nav">
						<li>
							<a href="" onClick={(e) => {e.preventDefault();this.props.newEntryModal('Debt');}}>
								<i className="fa fa-bolt"></i> <span>Nova despesa</span>
							</a>
						</li>
						<li>
							<a href="" onClick={(e) => {e.preventDefault();this.props.newEntryModal('Credit');}}>
								<i className="fa fa-money"></i> <span>Nova Receita</span>
							</a>
						</li>
						<li>
							<a href="" onClick={(e) => {e.preventDefault();this.props.newEntryModal('CreditCard');}}>
								<i className="fa fa-credit-card"></i> <span>Nova compra CC</span>
							</a>
						</li>
						<li onMouseLeave={() => this.changeOpen()}
							className={`dropdown user user-menu ${this.state.open ? 'open' : ''}`}>
							<a onClick={() => this.changeOpen()}
								aria-expanded={this.state.open ? 'true' : 'false'}
								className="dropdown-toggle" data-toggle="dropdown">
								<img src="https://static.thenounproject.com/png/17241-200.png" className="user-image" alt="" />
								<span className="hidden-xs">{name}</span> </a>
							<ul className="dropdown-menu">
								<li className="user-header">
									<img src="https://static.thenounproject.com/png/17241-200.png" className="img-circle" alt="" />
									<p>{name}<small>{email}</small></p>
								</li>
								<li className="user-footer"> <div className="pull-right">
									<a onClick={this.props.logout} className="btn btn-default btn-flat">Sair</a>
								</div> </li>
							</ul>
						</li>
					</ul>
				</div>
			</Fragment>
		);
	}
}
const mapStateToProps = state => ({ user: state.auth.user });
const mapDispatchToProps = dispatch => bindActionCreators({ logout, newEntryModal}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
