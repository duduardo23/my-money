import React from "react";

export default function Select(props) {
	return (
		<select
			{...props.input}
			className="form-control"
			readOnly={props.readOnly}
			disabled={props.readOnly}
		>
			{props.children}
		</select>
	);
}
