import axios from 'axios';
import consts from "../consts";

import { toastr } from 'react-redux-toastr';
import { getEntryApiUrl } from './util';

export function getCredits(year, month, page = 1, itemsPerPage = 10, tag) {
	const tagParam = tag ? '&tag=' + tag : '';
	const request = axios.get(`${consts.API_URL}/credit/${year}/${month}?page=${page}&itemsPerPage=${itemsPerPage}${tagParam}`);
	return {
		type: consts.CREDITS_FETCHED,
		payload: request
	};
}

export function getDebts(year, month, page = 1, itemsPerPage = 10, tag) {
	const tagParam = tag ? '&tag=' + tag : '';
	const request = axios.get(`${consts.API_URL}/debt/${year}/${month}?page=${page}&itemsPerPage=${itemsPerPage}${tagParam}`);
	return {
		type: consts.DEBTS_FETCHED,
		payload: request
	};
}

export function init() {
	return (dispatch, getState) => {
		const {month, year} = getState().cycle;
		dispatch(getDebts(year, month));
		dispatch(getCredits(year, month));
		dispatch(getMonthSummary(year, month));
	};
}

export function debtUpdated(debt){
	return {
		type: consts.DEBT_UPDATED,
		payload: debt
	};
}
export function creditUpdated(credit){
	return {
		type: consts.CREDIT_UPDATED,
		payload: credit
	};
}

export function updateDebt(id, data){
	return dispatch => {
		axios.put(`${consts.API_URL}/debt/${id}`, data)
			.then((resp) => {
				toastr.success('Sucesso', 'Operação executado com sucesso!');
				dispatch(debtUpdated(resp.data));
			}).catch(error => {
				const errors = error.response.data.errors || [];
				errors.forEach(error => {
					toastr.error('Erro', error);
				});
			});
	};
}

export function updateDebtStatus(id, newStaus){
	return dispatch => {
		axios.put(`${consts.API_URL}/debt/${id}/status`, {status: newStaus})
			.then((resp) => {
				toastr.success('Sucesso', 'Operação executado com sucesso!');
				dispatch(debtUpdated(resp.data));
			}).catch(error => {
				const errors = error.response.data.errors || [];
				errors.forEach(error => {
					toastr.error('Erro', error);
				});
			});
	};
}
export function updateCredit(id, data){
	return dispatch => {
		axios.put(`${consts.API_URL}/credit/${id}`, data)
			.then((resp) => {
				toastr.success('Sucesso', 'Operação executado com sucesso!');
				dispatch(creditUpdated(resp.data));
			}).catch(error => {
				const errors = error.response.data.errors || [];
				errors.forEach(error => {
					toastr.error('Erro', error);
				});
			});
	};
}

export function removeEntry(type, id){
	return dispatch => {
		axios.delete(getEntryApiUrl(type, id))
			.then((resp) => {
				toastr.success('Sucesso', 'Operação executado com sucesso!');
				dispatch(init());
			}).catch(error => {
				const errors = error.response.data.errors || [];
				errors.forEach(error => {
					toastr.error('Erro', error);
				});
			});
	};
}

export function changeMonth(month, year){
	return [{
		type: consts.CHANGE_MONTH,
		payload: {month, year}
	},init()];
}


export function getMonthSummary(year, month){
	const request = axios.get(`${consts.API_URL}/summary/${year}/${month}`);
	return {
		type: consts.MONTH_SUMMARY_FETCHED,
		payload: request
	};
}
