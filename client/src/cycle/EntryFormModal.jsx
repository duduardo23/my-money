import React, { Component } from "react";
import {Modal, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {hideModal} from '../modal/modalActions';
import axios from 'axios';
import consts from '../consts';
import { toastr } from 'react-redux-toastr';
import {formateDateToInput} from '../common/util/date';
import Row from '../common/layout/Row';
import Grid from '../common/layout/Grid';
import If from '../common/operator/If';
import {debtUpdated, creditUpdated} from './cycleActions';
import {getEntryApiUrl} from './util';

class EntryFormModal extends Component {
	state = {
		entryDate: '',
		value: '',
		name: '',
		tag: '',
		status: '',
		recurrent: false,
		recurrentCount: 0,
		recurrentTotal: 0
	}
	constructor(props){
		super(props);
		this.save = this.save.bind(this);
		this.onChange = this.onChange.bind(this);
	}
	componentWillMount(){
		if(!this.props.id){
			this.setState({ entryDate : formateDateToInput(new Date()) });
			return;
		}
		axios.get(getEntryApiUrl(this.props.type, this.props.id))
		.then(resp => {
			const dt = new Date(resp.data.entryDate);
			this.setState({ ...resp.data,
				entryDate : formateDateToInput(dt)
			});
		}).catch(error => {
			console.error(error);
			toastr.error('Erro', 'Não foi possivel ao executar operação: ' + error);
			this.props.hideModal();
		});
	}


	componentDidMount(){
		this.refs.inptEntryDate.select();
		this.refs.inptEntryDate.focus();
	}

	save(e){
		e.preventDefault();
		const data = {
			...this.state,
			recurrentCount: this.state.recurrentCount || 0,
			recurrentTotal: this.state.recurrentTotal || 0
		};
		const url = getEntryApiUrl(this.props.type, this.props.id);
		const method = (this.props.id) ? axios.put : axios.post;
		method(url, data)
			.then((resp) => {
				toastr.success('Sucesso', 'Operação executado com sucesso!');
				if(this.props.id){
					this.props.type === "Debt" ? this.props.debtUpdated(resp.data) : this.props.creditUpdated(resp.data);
				}
				this.props.hideModal();
			}).catch(error => {
				console.log(error);
				const errors = error.response.data.errors || [];
				errors.forEach(error => {
					toastr.error('Erro', error);
				});
			});
		console.log('save');
	}

	update(){

	}

	onChange(e){
		const target = e.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		const newState = {};
		newState[name] = value;
		this.setState(newState);
	}

	getFrindleType(){
		return {
			'Debt' : 'despesa',
			'Credit': 'receita',
			'CreditCard': 'cartão de credito'
		}[this.props.type];
	}

	modalTitle(){
		if(this.props.id){
			return `Editando ${this.getFrindleType()} ${this.props.id}`;
		}
		return `Novo(a) ${this.getFrindleType()}`;
	}

	render() {
		return (
			<div className="static-modal">
				<Modal.Dialog>
					<form className="" onSubmit={this.save}>
						<Modal.Header onHide={() => this.props.hideModal() } closeButton>
							<Modal.Title>{this.modalTitle()}</Modal.Title>
						</Modal.Header>
						<Modal.Body>
								<Row>
									<Grid cols="6">
										<div className="form-group">
											<label htmlFor="entryDate">Data</label>
											<input type="date" className="form-control" id="entryDate" placeholder="Data"
												value={this.state.entryDate} name="entryDate" onChange={this.onChange}
												ref="inptEntryDate"
											/>
										</div>
									</Grid>
									<Grid cols="6">
										<div className="form-group">
											<label htmlFor="value">Valor</label>
											<input type="number" className="form-control" id="value" placeholder="Valor"
												value={this.state.value} name="value" onChange={this.onChange}
												step="0.01" min="0" ref="inptValue" required
											/>
										</div>
									</Grid>
								</Row>
								<div className="form-group">
									<label htmlFor="name">Nome</label>
									<input type="text" className="form-control" id="entryDate" placeholder="Informa a descrição"
										value={this.state.name} name="name" onChange={this.onChange}
										required
									/>
								</div>
								<Row>
									<Grid cols="6">
										<div className="form-group">
											<label htmlFor="name">Tag</label>
											<input type="text" className="form-control" id="tag" placeholder="Informa a tag"
												value={this.state.tag} name="tag" onChange={this.onChange}
											/>
										</div>
									</Grid>
									<Grid cols="6">
										<If test={this.props.type === "Debt"}>
											<div className="form-group">
												<label htmlFor="status">Status</label>
												<select name="status" className="form-control" id="status"
													value={this.state.status} onChange={this.onChange}
													placeholder="Status">
													<option value="">Selecione...</option>
													<option value="PAYD">Pago</option>
													<option value="SCHEDULED">Agendado</option>
													<option value="PENDING">Pendente</option>
												</select>
											</div>
										</If>
									</Grid>
								</Row>

								<div className="form-group">
									<div className="checkbox">
										<label>
											<input type="checkbox" name="recurrent" className="th-value"
												checked={this.state.recurrent} onChange={this.onChange}
											/> Recorrente
										</label>
									</div>
								</div>
								<Row>
									<If test={this.state.recurrent}>
										<Grid cols="4">
											<div className="form-group">
												<label htmlFor="recurrentCount">Parcela atual</label>
												<input type="number" className="form-control"
												name="recurrentCount" id="recurrentCount"
												value={this.state.recurrentCount} onChange={this.onChange} />
											</div>
										</Grid>
										<Grid cols="4">
											<div className="form-group">
												<label htmlFor="recurrentTotal">Total de parcelas</label>
												<input type="number" className="form-control"
												name="recurrentTotal" id="recurrentTotal"
												value={this.state.recurrentTotal} onChange={this.onChange} />
											</div>
										</Grid>
									</If>
								</Row>
						</Modal.Body>

						<Modal.Footer>
							<Button onClick={() => this.props.hideModal() }>Cancelar</Button>
							<Button bsStyle="primary" type="submit">Salvar</Button>
						</Modal.Footer>
					</form>
				</Modal.Dialog>
			</div>
		);
	}
}


const mapDispatchToProps = (dispach) => {
	return bindActionCreators({ hideModal, debtUpdated, creditUpdated}, dispach);
};
export default connect(null, mapDispatchToProps)(EntryFormModal);

