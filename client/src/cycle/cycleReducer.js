import consts from "../consts";
const currentDate = new Date();
const INITIAL_STATE = {
	month: currentDate.getMonth() + 1,
	year: currentDate.getFullYear(),
	debts: {
		page: 1,
		itemsPerPage: 10,
		totalPages: 0,
		totalItems: 0,
		items:[],
		tag: ''
	},
	credits: {
		page: 1,
		itemsPerPage: 10,
		totalPages: 0,
		totalItems: 0,
		items:[],
		tag: ''
	},
	summary: {
		debt: 0,
		credit: 0
	}
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case consts.CREDITS_FETCHED:
			return {
				...state,
				credits: action.payload.data
			};
		case consts.DEBTS_FETCHED:
			return {
				...state,
				debts: action.payload.data
			};
		case consts.DEBT_UPDATED:
			return {
				...state,
				debts : {
					...state.debts,
					items: state.debts.items.map(debt => (debt.id === action.payload.id ? action.payload : debt))
				}
			};
		case consts.CREDIT_UPDATED:
			return {
				...state,
				credits : {
					...state.credits,
					items: state.credits.items.map(credit => (credit.id === action.payload.id ? action.payload : credit))
				}
			};
		case consts.CHANGE_MONTH:
			return {
				...state,
				month: action.payload.month,
				year: action.payload.year
			};
		case consts.MONTH_SUMMARY_FETCHED:
			return {
				...state,
				summary: action.payload.data
			};
		default:
			return state;
	}
};
