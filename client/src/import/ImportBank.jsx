import React, { Component } from "react";


import ContentHeader from "../common/template/ContentHeader";
import Content from "../common/template/Content";
import axios from "axios";
import consts from "../consts";

import { toastr } from 'react-redux-toastr';

class ImportBank extends Component {
	handleFileRead(e){
		const content = e.currentTarget.result;
		if(!content){
			return;
		}
		axios.post(`${consts.API_URL}/import/txt`,{
			lines: content.split('\n')
		}).then(resp => {
			let totaCredits = 0;
			let totaDebts = 0;
			if(resp.data.credits){
				totaCredits = resp.data.credits.length;
			}
			if(resp.data.debts){
				totaDebts = resp.data.debts.length;
			}
			toastr.success('Importação concluida.', `Foram importados ${totaCredits} créditos e ${totaDebts} débitos.`);
			console.log('items importados', resp);
		}).catch(e => {
			const errors = e.response.data.errors || [];
				errors.forEach(error => {
					toastr.error('Erro', error);
				});
		});
	}
	handleFileChoose(file){
		const fileReader = new FileReader();
		fileReader.onloadend = this.handleFileRead;
		fileReader.readAsText(file);
	}

	render() {
		return (
			<div>
				<ContentHeader
					title="Importação extrato"
					small="Importação extrato bancario em formato txt."
				/>
				<Content>
					<form>
						<div className="form-group">
							<label htmlFor="file">Arquivo txt</label>
							<input type="file" id="file" accept="*.txt"
								onChange={e => this.handleFileChoose(e.target.files[0])}/>
							<p className="help-block">
								Selecione o arquivo txt exportado no site do Itau.
							</p>
						</div>
					</form>
				</Content>
			</div>
		);
	}
}

export default ImportBank;

