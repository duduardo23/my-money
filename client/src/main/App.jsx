import React, { Component } from "react";
import Header from "../common/template/Header";
import SideBar from "../common/template/SideBar";
import Footer from "../common/template/Footer";
import Messages from "../common/msg/Messages";
import { HashRouter } from "react-router-dom";
import Routes from "./Routes";
import Loading from "../common/Loading/Loading";
import ModalRoot from "../modal/ModalRoot";

class App extends Component {
	render() {
		return (
			<HashRouter>
				<div className="wrapper">

					<Header />
					<SideBar />
					<div className="content-wrapper">
					<Loading/>
						<Routes />
					</div>
					<Footer />
					<Messages />
					<ModalRoot />
				</div>
			</HashRouter>
		);
	}
}
export default App;
