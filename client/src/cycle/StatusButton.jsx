import React, { Component } from 'react';
import {DropdownButton, MenuItem} from 'react-bootstrap';
class StatusButton extends Component {
	configurations = {
		PAYD: { className: "text-green", label: "Pago", style: "default"},
		PENDING: { className: "text-yellow", label: "Pendente", style: "warning"},
		SCHEDULED: { className: "text-light-bule", label: "Agendado", style: "info" },
		none: {className: "", label: "        ", style: "default"}
	};

	getOptions(){
		const {status} = this.props.item;
		const items = [];
		if(status !== 'PAYD'){
			items.push(<MenuItem key="PAYD"  eventKey="PAYD" onClick={() => this.props.updateStatus('PAYD')}>Pago</MenuItem>);
		}
		if(status !== 'PENDING'){
			items.push(<MenuItem key="PENDING" eventKey="PENDING" onClick={() => this.props.updateStatus('PENDING')}>Pendente</MenuItem>);
		}
		if(status !== 'SCHEDULED'){
			items.push(<MenuItem key="SCHEDULED" eventKey="SCHEDULED" onClick={() => this.props.updateStatus('SCHEDULED')}>Agendado</MenuItem>);
		}
		return items;
	}

	render() {
		const {id, status} = this.props.item;
		const conf = this.configurations[status || 'none'];
		return (
			<DropdownButton title={conf.label} id={`bg-status-${id}`} bsSize="xs" bsStyle={conf.style}>
				{this.getOptions()}
			</DropdownButton>
		);
	}
}

export default StatusButton;
