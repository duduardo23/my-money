const brlFormat = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' });

export default (value) => brlFormat.format(value);
