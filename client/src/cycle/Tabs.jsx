import React, { Component } from "react";
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import {changeMonth} from './cycleActions';
class Tabs extends Component {

	monthsName = {
		'1' : 'Jan', '2' : 'Fev', '3' : 'Mar', '4': 'Abr', '5': 'Mai', '6': 'Jun', '7': 'Jul', '8': 'Ago', '9': 'Set', '10' : 'Out', '11': 'Nov', '12': 'Dez'
	}

	renderMonths(){
		const months = [];
		const {month, year} = this.props;
		months.push(
			<li key={year - 1}>
				<a href="" onClick={(e) => {e.preventDefault(); this.props.changeMonth(month, year - 1);}}>
					<i className="fa fa-angle-double-left" aria-hidden="true"></i>&nbsp;{year - 1}
				</a>
			</li>
		);
		for (let i = 1; i <= 12; i++) {
			months.push(
				<li className={`${month === i ? 'active': ''}`} key={i}>
					<a href="" onClick={(e) => {e.preventDefault(); this.props.changeMonth(i, year);}}>
						{this.monthsName[i]}/{year}
					</a>
				</li>
			);
		}
		months.push(
			<li key={year + 1}>
				<a href="" onClick={(e) => {e.preventDefault(); this.props.changeMonth(month, year + 1);}}>
				{year + 1}&nbsp;<i className="fa fa-angle-double-right" aria-hidden="true"></i>
				</a>
			</li>
		);
		return months;
	}

	render() {
		return (
			<div className="nav-tabs-custom ">
				<ul className="nav nav-tabs nav-justified">
					{this.renderMonths()}
				</ul>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	month: state.cycle.month,
	year: state.cycle.year
});
const mapDispatchToProps = (dispach) => {
	return bindActionCreators({ changeMonth}, dispach);
};

export default connect(mapStateToProps, mapDispatchToProps)(Tabs);

