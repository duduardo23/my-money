export function formateDateToInput(date){
	const day = date.getDate();
	const month = date.getMonth() + 1;
	return `${date.getFullYear()}-${(month > 9 ? month : '0' + month)}-${(day > 9 ? day : '0' + day)}`;
}
