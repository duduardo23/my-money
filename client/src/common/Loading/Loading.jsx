import React, { Component } from 'react';
import './Loading.css';
import axios from 'axios';
class Loading extends Component {
	state = {
		loadingCount: 0
	}
	increase(){
		this.setState({loadingCount: this.state.loadingCount + 1});
	}

	decrease(){
		if(this.state.loadingCount <= 0){
			return;
		}
		this.setState({loadingCount: this.state.loadingCount - 1});
	}

	constructor(props){
		super(props);
		axios.interceptors.request.use((config) => {
			this.increase();
			return config;
		  }, (error) => {
			this.decrease();
			return Promise.reject(error);
		  });

		// Add a response interceptor
		axios.interceptors.response.use((response) => {
			this.decrease();
			return response;
		  }, (error) => {
			this.decrease();
			return Promise.reject(error);
		  });
	}
	render() {
		if(this.state.loadingCount <= 0){
			return null;
		}
		return (
			<div className="loading-div" >
				<div className="loading-content">
					<div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
					<span>Carregando...</span>
				</div>
			</div>
		);
	}
}

export default Loading;
