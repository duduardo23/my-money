import React, { Component } from "react";
import {
	LineChart,
	Line,
	XAxis,
	YAxis,
	CartesianGrid,
	Tooltip,
	Legend
} from "recharts";
import axios from "axios";
import consts from '../consts';

class CreditDebitChart extends Component {
	constructor(props) {
		super(props);
		this.state = { data: [] };
	}

	componentDidMount() {
		axios
			.get(`${consts.API_URL}/dashboard/summaryByMonth`)
			.then(resp => {
				const vlr = resp.data.map(v => {
					return {...v, res: (v.credit - v.debt).toFixed(2)};
				});
				this.setState({data: vlr});
			});
	}

	render() {
		return (
			<div className="box box-primary">
				<div className="box-header with-border">
					<h3 className="box-title">Resumo mensal</h3>
				</div>
				<div className="box-body">
					<div className="chart">
						<LineChart
						width={640}
						height={300}
						data={this.state.data}
						margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
					>
						<XAxis dataKey="name" />
						<YAxis />
						<CartesianGrid strokeDasharray="3 3" />
						<Tooltip />
						<Legend />
						<Line
							type="monotone"
							dataKey="credit"
							stroke="#82ca9d"
							activeDot={{ r: 8 }}
							name="Créditos"
						/>
						<Line
							type="monotone"
							dataKey="res"
							stroke="#0000FF"
							name="Consolidado"
						/>
						<Line
							type="monotone"
							dataKey="debt"
							stroke="#e50218"
							name="Débitos"
						/>
					</LineChart>
              </div>
            </div>
          </div>

		);
	}
}

export default CreditDebitChart;
