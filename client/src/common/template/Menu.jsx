import React, { Component } from "react";
import MenuItem from "./MenuItem";
import MenuTree from "./MenuTree";

class Menu extends Component {
	render() {
		return (
			<ul className="sidebar-menu">
				<MenuItem path="/" icon="dashboard" label="Dashboard" />
				<MenuItem path="/ciclo" icon="usd" label="Ciclos Mensais" />
				<MenuTree label="Cadastro" icon="edit">
					<MenuItem
						label="Ciclos de pagamento"
						path="billingCycles"
						icon="usd"
					/>
				</MenuTree>
				<MenuItem path="/importar-extrato" icon="upload" label="Importar..." />
			</ul>
		);
	}
}

export default Menu;
