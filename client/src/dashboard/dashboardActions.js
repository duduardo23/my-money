import axios from 'axios';

import consts from '../consts';

export const getSummary = () => {
	const request = axios.get(`${consts.API_URL}/dashboard/summary`);
	return {
		type: 'BILLING_SUMMARY_FETCHED',
		payload: request
	};
};
