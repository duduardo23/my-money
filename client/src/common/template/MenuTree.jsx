import React, { Component } from "react";

class MenuTree extends Component {
	constructor(props) {
		super(props);
		this.state = { open: false };
		this.changeOpen = this.changeOpen.bind(this);
	}
	changeOpen(event) {
		event.preventDefault();
		this.setState({ open: !this.state.open });
	}
	render() {
		return (
			<li
				className={`treeview ${this.state.open ? "active" : ""}`}
			>
				<a href="" onClick={this.changeOpen}>
					<i className={`fa fa-${this.props.icon}`} />
					<span>{this.props.label}</span>
					<i className="fa fa-angle-left pull-right" />
				</a>
				<ul className="treeview-menu">{this.props.children}</ul>
			</li>
		);
	}
}

export default MenuTree;
