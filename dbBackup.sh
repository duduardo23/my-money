#!/bin/bash

FOLDER=$( cd $(dirname $0) ; pwd -P )
BK_FOLDER=$FOLDER/backup


BK_FILE=$BK_FOLDER/db_$(date +%F_%R).sql

mkdir -p $BK_FOLDER

docker exec my-money_mariadb_1 /usr/bin/mysqldump -u root --password=42f5287658 mymoney > $BK_FILE
