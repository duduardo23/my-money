
import React, { Component } from 'react';

import Button from './Button';

class ButtonGroup extends Component {
	state = {
		open: false
	}
	render() {
		return (
			<div className={`btn-group ${this.state.open ? 'open' : ''}`}>
				<Button {...this.props}/>
				<Button {...this.props} className="dropdown-toggle" icon="" label="" onClick={() => this.setState({open: !this.state.open})}>
					<span className="caret"></span>
					<span className="sr-only">Toggle Dropdown</span>
				</Button>
				<ul className="dropdown-menu" role="menu" onMouseLeave={() => this.setState({open: false})}>
					<li>
						<a href="">Excluir</a>
					</li>
					<li>
						<a href="">Action 2</a>
					</li>
				</ul>
			</div>
		);
	}
}

export default ButtonGroup;
