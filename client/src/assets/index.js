import './jquery';
import 'admin-lte/bower_components/fastclick/lib/fastclick.js';
import 'admin-lte/bower_components/bootstrap/dist/js/bootstrap.min.js';
import 'admin-lte/dist/js/adminlte.min.js';

import 'font-awesome/css/font-awesome.css';
import 'ionicons/dist/css/ionicons.min.css';
import 'admin-lte/bower_components/bootstrap/dist/css/bootstrap.min.css';
import 'admin-lte/dist/css/AdminLTE.min.css';
import 'admin-lte/dist/css/skins/_all-skins.min.css';
import 'admin-lte/plugins/iCheck/flat/blue.css';

import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

import './css/index.css';
import './css/auth.css';
import './scss/index.scss';
