import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Grid from "../common/layout/Grid";
import Row from "../common/layout/Row";
import ContentHeader from "../common/template/ContentHeader";
import Content from "../common/template/Content";
import List from "./List";
import {init, getDebts, getCredits} from './cycleActions';
import Tabs from "./Tabs";
import ValueBox from '../common/widgets/ValueBox';
class Cycle extends Component {
	componentWillMount(){
		this.props.init(2018, 8);
	}
	renderSummary(){
		const {debt, credit} = this.props.summary;
		return (
			<Row>
				<ValueBox cols='2' color='red' icon='credit-card'
					value={debt} text='Total de Débitos' />
				<ValueBox cols='2' color='green' icon='bank'
					value={credit} text='Total de Créditos' />
				<ValueBox cols='2' color='blue' icon='money'
					value={credit - debt} text='Valor Consolidade' />
			</Row>
		);
	}
	render() {
		return (
			<div>
				<ContentHeader
					title="Ciclos mensais"
				/>
				<Tabs/>
				<Content>
					{this.renderSummary()}
					<Row>
						<Grid cols="6">
							<List title="Despesas" type="Debt" data={this.props.debts} boxStyle="danger"
								onChangeData={(page, itemsPerPage, tag) => this.props.getDebts(this.props.year, this.props.month, page, itemsPerPage, tag)}/>
						</Grid>
						<Grid cols="6">
							<List title="Receitas" type="Credit" data={this.props.credits} boxStyle="success"
								onChangeData={(page, itemsPerPage, tag) => this.props.getCredits(this.props.year, this.props.month, page, itemsPerPage, tag)}/>
						</Grid>
					</Row>
				</Content>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	debts: state.cycle.debts,
	credits: state.cycle.credits,
	summary: state.cycle.summary,
	month: state.cycle.month,
	year: state.cycle.year
});
const mapDispatchToProps = (dispach) => {
	return bindActionCreators({ init, getDebts, getCredits }, dispach);
};

export default connect(mapStateToProps, mapDispatchToProps)(Cycle);
